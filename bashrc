################
# PS1 with git branch name
################
function git-branch-name {
  git symbolic-ref HEAD 2>/dev/null | cut -d"/" -f 3
}
function git-branch-prompt {
  local branch=`git-branch-name`
  if [ $branch ]; then printf " \e[1;37mon\e[1;31m %s" $branch; fi
}

export PS1='\[\e[1;35m\][\[\e[1;33m\]\u\[\e[1;31m\]@\[\e[1;33m\]\h \[\e[1;31m\] \[\e[1;36m\]\t \[\e[1;32m\]\W$(git-branch-prompt)\[\e[1;35m\]]\[\e[1;34m\]\$ \[\e[0m\]'


################
# alias
################
alias ls='ls --color=auto'
alias python='python3'
alias pip='pip3'


################
# Golang
################
# export GOROOT=/usr/local/go
# export GOPATH=$HOME/go
# export PATH=$PATH:$GOPATH/bin
# export GO111MODULE=on


################
# git auto complete
################
if [ -f ~/.git-completion.bash ]; then
    . ~/.git-completion.bash
fi