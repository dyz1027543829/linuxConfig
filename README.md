# 快速拉起一个 user-friendly 的Linux环境

## Including

- 更新source.list
- 基础软件工具
- vim插件安装
- zshrc + oh-my-zsh第三方插件安装
- sources.list
- git配置
- macos
    - mac brew加速安装
    - goland
    - idea
- 相关工具安装

Enjoy it!

# 更新source.list

> 此步骤仅适用于ubuntu18和ubuntu20，低于ubuntu18的版本对应的source.list自行查找。centos无需更新此文件

首先查看系统版本或系统代号：

```bash
cat /etc/os-release

lsb_release -a
```

如果是ubuntu18.04，则发行代号为bionic，如果是ubuntu20.04，发行代号为focal

更新source.list最好将之前的备份一下：`sudo cp /etc/apt/sources.list /etc/apt/sources.list.bak`

在sourcelist目录找到合适的source.list，复制内容覆盖到 /etc/apt/source.list，然后执行 `sudo apt update`

# 基础软件工具安装

这里只列出来，具体可以自己找教程安装，不过基本都可以直接  apt install 一键安装

- net-tools
- git
- vim（版本至少8）
- zip、unzip
- tree
- wget
- go（这个直接到官网上下载解压，配置下环境变量和go env即可）
- docker（进官网找安装方法，记得安装的是 docker engine，不是 docker 桌面版！）

# Vim configuration

```bash
cp ./vim/vimrc ~/.vimrc

mkdir ~/.vim

cp -r ./vim/autoload ~/.vim/
```

先安装 ctags-exuberant，因为TagList插件需要这个才能工作，安装方法如下：

先安装编译工具：

```bash
sudo apt install gcc
sudo apt install make
```

再安装ctags：

```bash
tar zxf ctags-5.8.tar.gz

cd ctags-5.8

./configure

make

sudo make install
```

最后，打开vim，安装所有插件

`:PlugInstall`

等待安装完成即可


# git配置

```bash
git config --global alias.co checkout
git config --global alias.br branch
git config --global alias.ci commit
git config --global alias.st status
git config --global alias.last 'log -1 --stat HEAD'
git config --global alias.lg "log --graph --pretty=format:'%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset'"
git config --global alias.count "shortlog --summary --numbered"
git config --global core.editor vim
# 禁止git branch命令触发less分页
git config --global pager.branch false
```


# ZSH+OMZ

## 安装zsh

```bash
sudo apt install zsh
```

## 安装oh-my-zsh

```bash
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

会自动将zsh作为默认的shell，并生成一个默认的.zshrc配置文件

接下来使用本仓库的zshrc文件覆盖默认的zshrc

```bash
rm ~/.zshrc
cp zshrc ~/.zshrc
```

先不要急着source，会报错，source报错了也先别急

## 下载oh-my-zsh第三方插件

### zsh-autosuggestions

```bash
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
```

### zsh-syntax-highlighting

```bash
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
```

## Fuzzy Finder (fzf)

推荐使用git下载并安装，不要使用linux自带的apt工具，版本太老

```bash
# 可以修改fzf的目录，同时也要修改zshrc中的配置
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf

# 前两个都输入y，最后问是否修改zshrc，选择n
~/.fzf/install
```

## ranger

一个vim风格的文件夹浏览工具

https://github.com/ranger/ranger

按照README中的步骤来安装即可，可能需要python环境


## 修改zshrc

根据需要进行修改即可，可以修改的位置直接搜索`(opt)`即可，包括：

- PATH
- 主题
- 插件和插件特定的配置（如docker、fzf、kubectx）
- 是否启用已经存在的bashrc
- 用户自定义配置（SSH alias、GO环境变量等）

最后别忘了将配置生效，不要使用`source ~/.zshrc`，这是错误的做法，正确的做法是：

```bash
omz reload
```

# Glow

Glow是一款非常好用的命令行markdown阅读器

1. 安装gpg

```bash
sudo apt install gpg
```

2. 将glow的密钥环添加到apt源

```bash
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://repo.charm.sh/apt/gpg.key | sudo gpg --dearmor -o /etc/apt/keyrings/charm.gpg
echo "deb [signed-by=/etc/apt/keyrings/charm.gpg] https://repo.charm.sh/apt/ * *" | sudo tee /etc/apt/sources.list.d/charm.list
```

3. 安装glow

```bash
sudo apt update
sudo apt install glow
```

# Tmux

命令行复用和会话粘滞工具

1. 安装

```bash
sudo apt install tmux
```

2. 使用参考[Tmux教程](https://www.ruanyifeng.com/blog/2019/10/tmux.html)

3. （可选）安装[tmuxinator](https://github.com/tmuxinator/tmuxinator)

4. 开启zshrc插件：tmux、tmuxinator（可选）

# macos brew 安装

使用国内镜像来安装，更快：

```bash
/bin/zsh -c "$(curl -fsSL https://gitee.com/huwei1024/HomebrewCN/raw/master/Homebrew.sh)"
```

# 有点意思的工具（opt）

## Smug

tmux管理工具

https://github.com/ivaaaan/smug

```bash
cd /tmp
git clone https://github.com/ivaaaan/smug.git
cd smug
go install
```

## viddy

更强大的watch命令

https://github.com/sachaos/viddy

```bash
go install github.com/sachaos/viddy@latest
```

## gotop

更强大的top命令

https://github.com/xxxserxxx/gotop

```bash
go install github.com/xxxserxxx/gotop/v4/cmd/gotop@latest
```

## docui

docker可视化管控

https://github.com/skanehira/docui.git

## laydocker

docker可视化管控

https://github.com/jesseduffield/lazydocker


## k9s

强大的k8s可视化管控工具

https://github.com/derailed/k9s

## go-sniffer

调试工具，嗅探数据库请求、解析、打印

https://github.com/40t/go-sniffer

## duf

更好的df工具，足以替代df

https://github.com/muesli/duf

## wtf

命令行个人看板工具

https://github.com/wtfutil/wtf

## fx

交互式或流式的JSON查看工具

https://github.com/antonmedv/fx

## 1Panel

Linux服务器运维管理面板

https://github.com/1Panel-dev/1Panel
