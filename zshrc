###################################################################################
########################## BASH Path Configuration (opt) ##########################

# export PATH=$HOME/bin:/usr/local/bin:$PATH
# (opt)
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

########################## BASH Path Configuration (opt) ##########################
###################################################################################


################################################################################
########################## OMZ specific configuration ##########################

### ZSH variables ###
export ZSH="$HOME/.oh-my-zsh"
# ZSH_CUSTOM=/path/to/new-custom-folder

### ZSH theme (opt) ###
ZSH_THEME="robbyrussell"
#ZSH_THEME="fino-time"
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment one of the following lines to change the auto-update behavior
# zstyle ':omz:update' mode disabled  # disable automatic updates
# zstyle ':omz:update' mode auto      # update automatically without asking
zstyle ':omz:update' mode reminder  # just remind me to update when it's time

# Uncomment the following line to change how often to auto-update (in days).
zstyle ':omz:update' frequency 60

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# You can also set it to another string to have that shown instead of the default red dots.
COMPLETION_WAITING_DOTS="%F{yellow}waiting...%f"
# Caution: this setting can cause issues with multiline prompts in zsh < 5.7.1 (see #5765)
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
HIST_STAMPS="yyyy-mm-dd"

########################## OMZ specific configuration ##########################
################################################################################


###########################################################################
########################## Plugin configurations ##########################

autoload -Uz compinit
compinit

### Plugins (opt) ###
# Add wisely, as too many plugins slow down shell startup.
#plugins=(zsh-syntax-highlighting zsh-autosuggestions ufw sudo z cp extract command-not-found safe-paste themes fzf zsh-interactive-cd aliases kubectl kubectx helm istioctl terraform git pip golang docker docker-compose virtualenv macos tmux vagrant)
plugins=(zsh-syntax-highlighting zsh-autosuggestions ufw sudo z cp command-not-found safe-paste themes fzf zsh-interactive-cd aliases kubectl)

### Plugin specific configuration (opt) ###
# docker
zstyle ':omz:plugins:docker' legacy-completion yes
zstyle ':completion:*:*:docker:*' option-stacking yes
zstyle ':completion:*:*:docker-*:*' option-stacking yes

# fzf
export FZF_BASE=$HOME/fzf
#export FZF_DEFAULT_COMMAND='<your fzf default command>'
#DISABLE_FZF_AUTO_COMPLETION="true"
#DISABLE_FZF_KEY_BINDINGS="true"

# kubectx (opt)
#export RPS1='%F{green}ctx: $(kubectx_prompt_info)%f'
#kubectx_mapping[kubernetes-admin@cluster.local]="console"

########################## Plugin configurations ##########################
###########################################################################



# enable oh-my-zsh and bashrc
source $ZSH/oh-my-zsh.sh
# (opt)
#source ~/.bashrc


########################################################################
########################## User configuration ##########################

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
alias zshrc='vim ~/.zshrc'
alias ll="ls -l"
alias la="ls -lAFh"
alias lS="ls -1FSsh"
alias rm="rm -i"
alias cp="cp -i"
alias mv="mv -i"
alias grep="grep --color"
alias sgrep="grep -R -n -H -C 5 --exclude-dir={.git,.svn,CVS}"
alias python="python3"
alias pip="pip3"

# Make zsh know about hosts already accessed by SSH
zstyle -e ':completion:*:(ssh|scp|sftp|rsh|rsync):hosts' hosts 'reply=(${=${${(f)"$(cat {/etc/ssh_,~/.ssh/known_}hosts(|2)(N) /dev/null)"}%%[# ]*}//,/ })'

# ssh connection (opt)
#alias jumpbox='ssh frankie@10.24.25.13 -p 22'
#alias devbox0='ssh -o TCPKeepAlive=yes openpier@10.24.23.222 -p 30050'
#alias devbox1='ssh openpie@10.24.23.222 -p 22'
#alias stage-center='ssh ubuntu@10.24.23.153 -p 22'   // tsp
#alias stage-region='ssh ubuntu@10.24.23.155 -p 22'   // tsp
#alias stable-center='ssh ubuntu@10.24.23.70 -p 22'   // openpie@tsp@70
#alias stable-region='ssh ubuntu@10.24.23.60 -p 22'         // openpie@tsp@60

# GO (opt)
# Golang
#export GOROOT=/usr/local/go
#export GOPATH=$HOME/.gopath
#export PATH=$PATH:$GOROOT/bin:$GOPATH/bin
#export GO111MODULE=on

# ZPLUG (opt)
#export ZPLUG_HOME=/opt/homebrew/opt/zplug
#source $ZPLUG_HOME/init.zsh

########################## User configuration ##########################
########################################################################
